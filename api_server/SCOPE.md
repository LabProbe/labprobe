The API_Server is intended to build a rudimentary HTTP/REST style API using the fastapi python library to interface with the NoSQL back-end of the LabProbe cloud platform.

Functions should include:

POST of new JSON records with Hash & Range Keys
*	Response with success or failure and how long it took to push

GET of all records between X and Y ranges (Scan)
*	Response of success or failure and how long it took to push.

GET of specific records between X and Y ranges (Query)
*	Response List of multiple JSON objects

GET of latest records of specific hash keys (i.e. most recent record for DeviceA and DeviceB where DeviceA and DeviceB are the Hash key)
*	Response List of 1 or more JSON objects (1 or more DeviceIDs)

PUT of new record which checks if the Hash and Range key already exists, if not creates
*	Response of if Object Exists
*	Whether it is different
**		Updates if it is different
		Does nothing if it isn't
*	Creates if it doesn't exist
