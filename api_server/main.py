from fastapi import FastAPI
import os
import json
from datetime import datetime
import boto3


def init_folders():
    global config_location
    # Initialize configuration and data logging folders.
    config_location = os.path.join(os.path.expanduser('~'), '.config', 'labprobe_api',)
    if not os.path.exists(config_location):
        print("Creating" + str(os.path.expanduser('~')) + "/.config/labprobe_api/ directory.")
        os.makedirs(config_location)
    else:
        print("Found aws configuration folder at: " + str(config_location))


def init_server_dynamo_config():
    global server_configuration
    global dynamodb
    try:
        with open(os.path.join(config_location, 'server_credentials.json'), 'r') as server_config:
            config = json.loads(server_config.read())
    except FileNotFoundError:
        print("No AWS DynamoDB configuration found.  Please enter it now now.")
        server_configuration = {}
        server_configuration['region_name'] = input("AWS Region: ")
        server_configuration['aws_access_key_id'] = input("AWS Access Key ID: ")
        server_configuration['aws_secret_access_key'] = input("AWS Access Secret Key ID: ")
        server_configuration['data_db'] = input("DynamoDB Experiment Data Table Name: ")
        server_configuration['metadata_db'] = input("DynamoDB Experiment Metadata Table Name: ")
        server_configuration['configuration_db'] = input("DynamoDB Configuration Table Name: ")
        with open(os.path.join(config_location, 'aws_credentials.json'), 'w') as config:
            config.write(json.dumps(server_configuration, indent=4))
    dynamodb = boto3.resource('dynamodb', region_name=server_configuration['region_name'], aws_access_key_id=server_configuration['aws_access_key_id'], aws_secret_access_key=server_configuration['aws_secret_access_key'])


app = FastAPI()


@app.get("/test")
async def read_root():
    data = {"Testing": 123}
    return data


@app.put("/v1/json")
async def put_data():

    return data

init_folders()
init_server_dynamo_config()
