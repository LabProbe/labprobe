from fastapi import FastAPI, Response, status
from fastapi.responses import JSONResponse
import os
import time
import datetime
import pytz
import simplejson as json
import tqdm
from decimal import Decimal
import boto3

server_configuration = {
    'region_name': 'us-west-2',
    'aws_acces_key_id': '',
    'aws_secret_access_key': '',
    'data_db': 'miprobe',
    'metadata_db': 'experiment_meta',
    'configuration_db': 'configuration_info',
    'timeout': 5
}

dynamodb = boto3.resource(
    'dynamodb',
    region_name=server_configuration['region_name'],
    aws_access_key_id=server_configuration['aws_acces_key_id'],
    aws_secret_access_key=server_configuration['aws_secret_access_key']
)

dynamodb.meta.client.meta.config.connect_timeout = 5


# using init_folders() and init_server_dynamo_config() to setup install on dedicated server systems.
def init_folders():
    global config_location
    # Initialize configuration and data logging folders.
    config_location = os.path.join(os.path.expanduser('~'), '.config', 'labprobe_api',)
    if not os.path.exists(config_location):
        print("Creating" + str(os.path.expanduser('~')) + "/.config/labprobe_api/ directory.")
        os.makedirs(config_location)
    else:
        print("Found aws configuration folder at: " + str(config_location))


def init_server_dynamo_config():
    global server_configuration
    global dynamodb
    try:
        with open(os.path.join(config_location, 'server_credentials.json'), 'r') as server_config:
            config = json.loads(server_config.read())
    except FileNotFoundError:
        print("No AWS DynamoDB configuration found.  Please enter it now now.")
        server_configuration = {}
        server_configuration['region_name'] = input("AWS Region: ")
        server_configuration['aws_access_key_id'] = input("AWS Access Key ID: ")
        server_configuration['aws_secret_access_key'] = input("AWS Access Secret Key ID: ")
        server_configuration['data_db'] = input("DynamoDB Experiment Data Table Name: ")
        server_configuration['metadata_db'] = input("DynamoDB Experiment Metadata Table Name: ")
        server_configuration['configuration_db'] = input("DynamoDB Configuration Table Name: ")
        server_configuration['timeout'] = input("Set DynamoDB Timeout Value: ")
        with open(os.path.join(config_location, 'aws_credentials.json'), 'w') as config:
            config.write(json.dumps(server_configuration, indent=4))
    dynamodb = boto3.resource(
        'dynamodb',
        region_name=server_configuration['region_name'],
        aws_access_key_id=server_configuration['aws_access_key_id'],
        aws_secret_access_key=server_configuration['aws_secret_access_key']
    )


def upload_data(data_dict, table_name):
    data_dict = json.loads(data_dict, parse_float=Decimal)
    table = dynamodb.Table(table_name)
    table.put_item(Item=data_dict)


# Basic Libraries for logging and string operations
def timestamp_8601(time):
    t = time
    date = str(zfl(str(t[0]), 4) + "-" + zfl(str(t[1]), 2) + "-" + zfl(str(t[2]), 2))
    time = str(zfl(str(t[3]), 2) + ":" + zfl(str(t[4]), 2) + ":" + zfl(str(t[5]), 2))
    return str(date + " " + time)


def zfl(s, width):
    '''Replaces .zfill on string for python and micropython compatibiity'''
    return '{:0>{w}}'.format(s, w=width)


def xprint(*args, **kwargs):
    '''Print Wrapper to add timestamp and hostname to print or tqdm.write() function.'''
    t = time.localtime()
    prefix = "[" + timestamp_8601(t) + "]"
    try:
        tqdm.write(prefix, *args, **kwargs)
    except Exception:
        print(prefix, *args, **kwargs)


def get_timezone():
    tz = str(time.tzname[time.daylight])
    if tz == ('PDT' or 'PST'):
        tz = "America/Los_Angeles"
    return tz


def timestamp(tz):
    tz = pytz.timezone(tz)
    utcnow = datetime.datetime.utcnow()
    ts = utcnow.replace(tzinfo=pytz.utc)
    lt = ts.astimezone(tz)

    time_dict = {
        "Timestamp": ts.strftime("%Y-%m-%d %H:%M:%S%z"),
        "LocalTime": lt.strftime("%Y-%m-%d %H:%M:%S%z"),
        "Datetime": int(ts.strftime("%s")) * 1000
    }

    return time_dict


def validate_payload(payload):
    errors = {}
    keys = ['Timestamp', 'DeviceID', 'ExperimentID']
    for i in keys:
        if i in payload.keys():
            continue
        else:
            errors[i] = ['Missing Key']
    if len(errors) == 0:
        return 1
    if len(errors) > 0:
        return errors


app = FastAPI()


@app.get("/test")
async def read_root():
    data = {"Testing": 123}
    return data


@app.put("/v1/miprobe_data/json")
async def put_data(miprobe_data: dict):
    miprobe_data['PayloadTime'] = timestamp('UTC')['Timestamp']
    if validate_payload(miprobe_data) is True:
        xprint("Valid dataset provided... uploading.")
        try:
            #upload_data(miprobe_data, server_configuration['data_db'])
            response = {"Message": "Data Received", "Timestamp": miprobe_data['PayloadTime']}
            return 
        except Exception as e:
            xprint(e)
            response = {"Error": str(e), "PayloadTime": miprobe_data['PayloadTime']}
            return response
    else:
        errors = validate_payload(miprobe_data)
        xprint("Invalid datasrt provided.  Check configuration and try again.")
        response  = JSONResponse(status_code=status.HTTP_400_BAD_REQUEST, content=errors)
        return response
