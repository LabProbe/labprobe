from datetime import datetime
import boto3
import pandas as pd
import os
import json

def init_folders():
    global config_location
    '''Initialize configuration and data logging folders.'''
    config_location = os.path.join(os.path.expanduser('~'), '.config', 'labprobe',)
    if not os.path.exists(config_location):
        print("Creating" + str(os.path.expanduser('~')) + "/.config/labprobe/ directory.")
        os.makedirs(config_location)
    else:
        print("Found aws configuration folder at: " + str(config_location))


def init_dynamo_config(default_index_columns, 
                        filename="aws_credentials.json", 
                        range_columns=['Timestamp', 'LocalTime'], 
                        environmental_config=True, 
                        metadata_config=False):
    global aws_configuration
    global dynamodb
    global tables
    global index_keys
    global range_keys
    global default_index_values

    range_keys = range_columns
    default_index_values = default_index_columns

    if filename == "aws_credentials.json":
        message = "Found default LabProbe Cloud configuration file " + filename + " in configuration folder"
    else:
        message = "Found specified LabProbe Cloud configuration file " + str(filename) + " in configuration folder."
    try:
        with open(os.path.join(config_location, filename), 'r') as configuration:
            print(message)
            aws_configuration = json.loads(configuration.read())
    except FileNotFoundError:
        print("No AWS DynamoDB configuration found.  Please enter it now now.")
        aws_configuration = {}
        aws_configuration['region_name'] = input("AWS Region: ")
        aws_configuration['aws_access_key_id'] = input("AWS Access Key ID: ")
        aws_configuration['aws_secret_access_key'] = input("AWS Access Secret Key ID: ")
        tables = input("Enter comma separated DynamoDB table names: ")
        aws_configuration['tables'] = tables.split(',')

        print("Hint: DynamoDB table hash keys are typically your index columns.")
        index_keys = input("Enter comma separated DynamoDB table hash keys: ")
        aws_configuration['hash_keys'] = index_keys.split(',')
        if range_keys == ['Timestamp', 'LocalTime']:
            print("Using default DynamoDB range keys of \'Timestamp\' and \'LocalTime\'.")
        else:
            print("Using custom range_keys " + str(range_keys))
        aws_configuration['range_keys'] = range_keys
        aws_configuration['default_index_values'] = default_index_values
        if environmental_config == True:
            global environmental_table
            global environmental_hash_key
            global environmental_range_key
            print("Hint: Environmental Metadata is usually a shared weather or lab environment conditions table.")
            environmental_table = input("Enter DynamoDB environmental metadata table name: ")
            environmental_table_hash_key = input("Enter DynamoDB comma-separated environmental metadata hash key: ")
            environmental_table_range_key = input("Enter DynamoDB comma-separated environmental metadata range key: ")
            aws_configuration['environmental_metadata_table'] = environmental_table
            aws_configuration['environmental_metadata_hash_keys'] = environmental_table_hash_key.split(',')
            aws_configuration['environmental_metadata_range_keys'] = environmental_table_range_key.split(',')
        if metadata_config == True:
            print("Hint: Regular Metadata is usually a table containing sensor instrument or lab analysis information such as instrumentation or protocols.")
            global metadata_table
            global metadata_hash_key
            global metadata_range_key
            metadata_table = input("Enter DynamoDB metadata table name: ")
            metadata_table_hash_key = input("Enter comma-separated DynamoDB metadata hash key(s): ")
            metadata_table_range_key = input("Enter comma-separated DynamoDB metadata range key(s): ")
            aws_configuration['metadata_table'] = metadata_table
            aws_configuration['metadata_hash_keys'] = metadata_table_hash_key.split(',')
            aws_configuration['metadata_range_keys'] = metadata_table_range_key.split(',')
        with open(os.path.join(config_location, filename), 'w') as config:
            config.write(json.dumps(aws_configuration, indent=4))

    dynamodb = boto3.resource('dynamodb', region_name=aws_configuration['region_name'], aws_access_key_id=aws_configuration['aws_access_key_id'], aws_secret_access_key=aws_configuration['aws_secret_access_key'])
    return aws_configuration


def query_table(key, name, start, end, table_name, time_column):
    Key = boto3.dynamodb.conditions.Key
    table = dynamodb.Table(table_name)
    data = []
    response = table.query(KeyConditionExpression=Key(key).eq(name) & Key(time_column).between(start, end))
    for i in response[u'Items']:
        data.append(i)
    return data
