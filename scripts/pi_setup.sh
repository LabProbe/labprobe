#!/bin/sh

sudo apt install tmux screen zsh jupyter micro 

curl -s https://install.zerotier.com | sudo bash 

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

sudo apt install python3-numpy python3-pandas python3-seaborn python3-matplotlib python3-serial python3-tqdm python3-simplejson python3-boto3

pip3 install miprobe azcati
