# Syringe Pump Library v0.1
# Tested on Cole-Parmer Model 111 Dual Syringe Pump over USB Serial
# See https://archive-resources.coleparmer.com/Manual_pdfs/74905%20Manual.pdf for serial command documentation
# Author Evan Taylor - Evan@EvanTaylor.Pro
import time
import os
import serial
import serial.tools.list_ports

def init(*args):
    global ser
    global serial_port
    serial_ports = serial.tools.list_ports.comports()
    ports = []

    for i, p in enumerate(serial_ports):
        if "ttyacm" in str(p).lower():
            ports.append(str(p).split(" ")[0])
        if "cu.usbmodem" in str(p).lower():
            ports.append(str(p).split(" ")[0])
        if os.name == 'nt':
            if "com" in str(p).lower():
                ports.append(str(p).split(" ")[0])

    if len(ports) == 0:
        print("No Serial Devices Found.  Check USB & Power Connections")
    elif len(ports) >= 2:
        print("Multiple devices found.  Please select Cole-Parmer Serial Port.")
        print("Port Number\t" + "Port")
        for index, port in enumerate(ports):
            print(str(index) + " \t\t" + str(port))
        selected_port = int(input("Enter Port Number: "))
        serial_port = str(ports[selected_port])
    else:
        print("Cole-Parmer Device found on port: " + str(ports[0]) + "\n")
        serial_port = str(ports[0])

    baud_rate = 115200

    ser = serial.Serial(serial_port, baud_rate)
    if ser.isOpen():
        ser.flushInput()
        ser.flushOutput()
        print("Serial connection established with Cole-Parmer Syringe Pump.\n")
    else:
        print("Cannot establish connection with " + str(serial_port) + ".")
        print("Check if another application is useding the serial port.\n")


def set_diameter_svolume_rate(diameter, syringe_volume, rate):
    size = str(diameter) + "mm" + "\r\n"

    ser.write(size.encode('ascii'))
    time.sleep(.5)
    print("Set Syringe Diameter: " + str(size))

    volume = str(syringe_volume) + "ul" + "\r\n"
    ser.write(volume.encode('ascii'))
    time.sleep(.5)
    print("Set Syringe Volume: " + str(volume))

    rate = "irate[min " + str(rate) + " ml/hr]" + "\r\n"
    ser.write(rate.encode('ascii'))
    time.sleep(.5)
    print("Set Rate: " + str(rate).strip("irate[min "))


def start():
    command = "run\r\n"
    ser.write(command.encode('ascii'))
    print("Started Syringe Pump Infusion.")


def start_reverse():
    command = "rrun\r\n"
    ser.write(command.encode('ascii'))
    print("Started Syringe Pump Extraction.")


def stop():
    command = "stop\r\n"
    ser.write(command.encode('ascii'))
    print("Stopped Syringe Pump.")


def get_crate():
    command = "crate\r\n"
    ser.write(command.encode('ascii'))
    time.sleep(0.5)
    response = str(ser.read(ser.inWaiting()).decode()).strip(">").strip("\n").strip("\r")
    time.sleep(0.5)
    ser.flushOutput()
    ser.flushInput()
    return response


def get_irate():
    command = "irate\r\n"
    ser.write(command.encode('ascii'))
    time.sleep(0.5)
    response = str(ser.read(ser.inWaiting()).decode()).strip(">").strip("\n").strip("\r")
    time.sleep(0.5)
    ser.flushOutput()
    ser.flushInput()
    return response


def get_wramp():
    command = "wramp\r\n"
    ser.write(command.encode('ascii'))
    time.sleep(0.5)
    response = str(ser.read(ser.inWaiting()).decode()).strip(">").strip("\n").strip("\r")
    time.sleep(0.5)
    ser.flushOutput()
    ser.flushInput()
    return response


def get_svolume():
    command = "svolume\r\n"
    ser.write(command.encode('ascii'))
    time.sleep(0.5)
    response = str(ser.read(ser.inWaiting()).decode()).strip(">").strip("\n").strip("\r")
    time.sleep(0.5)
    ser.flushOutput()
    ser.flushInput()
    return response


def get_tvolume():
    command = "tvolume\r\n"
    ser.write(command.encode('ascii'))
    time.sleep(0.5)
    response = str(ser.read(ser.inWaiting()).decode()).strip(">").strip("\n").strip("\r")
    time.sleep(0.5)
    ser.flushOutput()
    ser.flushInput()
    return response


def get_ivolume():
    command = "ivolume\r\n"
    ser.write(command.encode('ascii'))
    time.sleep(0.5)
    response = str(ser.read(ser.inWaiting()).decode()).strip(">").strip("\n").strip("\r")
    time.sleep(0.5)
    ser.flushOutput()
    ser.flushInput()
    return response

def get_wrate():
    command = "wrate\r\n"
    ser.write(command.encode('ascii'))
    time.sleep(0.5)
    response = str(ser.read(ser.inWaiting()).decode()).strip(">").strip("\n").strip("\r")
    time.sleep(0.5)
    ser.flushOutput()
    ser.flushInput()
    return response


def get_diameter():
    command = "diameter\r\n"
    ser.write(command.encode('ascii'))
    time.sleep(0.5)
    response = str(ser.read(ser.inWaiting()).decode()).strip(">").strip("\n").strip("\r").strip(">").strip("\n").strip("\r")
    time.sleep(0.5)
    ser.flushOutput()
    ser.flushInput()
    return response
