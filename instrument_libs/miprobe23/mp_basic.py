# Basic file/IO functions
import os
import time
import datetime
import pytz
import json
import csv
from tqdm import tqdm
import urllib3


def init_files():
    global data_location
    global config_location
    # Initialize configuration and data logging folders.
    config_location = os.path.join(os.path.expanduser('~'), '.config', 'labprobe')
    if not os.path.exists(config_location):
        xprint("Creating" + str(os.path.expanduser('~')) + "/.config/labprobe/ directory.")
        os.makedirs(config_location)
    else:
        xprint("Found configuration folder at: " + str(config_location))
    data_location = os.path.join(os.path.expanduser('~'), 'lp_data')
    if not os.path.exists(data_location):
        xprint("Creating" + str(os.path.expanduser('~')) + "/lp_data/ directory")
        os.makedirs(data_location)
    else:
        xprint("Found data folder at: " + str(data_location))


def init_keys():
    global api_key, api_url
    # Initialize API Key File
    try:
        with open(os.path.join(config_location, 'API.key')) as key:
            api_key = key.read()
    except FileNotFoundError:
        xprint("No API key file found.  Enter API Key and press return.")
        api_key = input("API Key: ")
        api_key = str(api_key)
        with open(os.path.join(config_location, 'API.key'), 'w') as key:
            key.write(api_key)
        pass
    # Initialize API URL File
    try:
        with open(os.path.join(os.path.expanduser('~'), '.config', 'labprobe', 'API.url')) as url:
            api_url = url.read()
    except FileNotFoundError:
        xprint("No API key file found.  Enter API URL and press return.")
        api_url = input("API URL: ")
        api_url = str(api_url)
        with open(os.path.join(os.path.expanduser('~'), '.config', 'labprobe', 'API.url'), 'w') as key:
            key.write(api_url)
        pass
    return api_key, api_url


def upload_data(data, experiment_id, api_key, api_url):
    data['ExperimentName'] = experiment_id
    timeout = urllib3.Timeout(connect=10.0, read=10.0)
    http = urllib3.PoolManager(timeout=timeout)
    payload = json.dumps(data, indent=4, sort_keys=True)
    http.request('POST', api_url, headers={'Content-Type': 'application/json', 'x-api-key': api_key}, body=payload)


def dict_to_csv(data, filename):
    completeName = os.path.join(data_location, filename)
    with open(completeName, 'a') as f:
        w = csv.DictWriter(f, data.keys())
        if f.tell() == 0:
            w.writeheader()
            w.writerow(data)
        else:
            w.writerow(data)


def json_from_csv(filename):
    data = []
    with open(os.path.join(data_location, filename), 'r') as csvfile:
        csvreader = csv.DictReader(csvfile)
        for item in csvreader:
            data.append(item)
    return data


def dict_to_jsonfile(data_dict, filename):
    json_list = []
    file = os.path.join(data_location, filename)
    if not os.path.isfile(file):
        json_list.append(data_dict)
        with open(file, mode='w', encoding='utf-8') as f:
            f.write(json.dumps(json_list, indent=4))

    else:
        with open(file) as feedsjson:
            feeds = json.load(feedsjson)

        feeds.append(data_dict)
        with open(file, mode='w') as f:
            f.write(json.dumps(feeds, indent=4))


def log_error(filename, error):
    completeName = os.path.join(data_location, filename)
    ts = datetime.datetime.now()
    errorDict = {
        "Timestamp": ts.strftime("%Y-%m-%d %H:%M:%S"),
        "Error": str(error)
    }
    dict_to_csv(errorDict, completeName)


def timestamp_8601(time):
    t = time
    date = str(zfl(str(t[0]), 4) + "-" + zfl(str(t[1]), 2) + "-" + zfl(str(t[2]), 2))
    time = str(zfl(str(t[3]), 2) + ":" + zfl(str(t[4]), 2) + ":" + zfl(str(t[5]), 2))
    return str(date + " " + time)


def zfl(s, width):
    '''Replaces .zfill on string for python and micropython compatibiity'''
    return '{:0>{w}}'.format(s, w=width)


def xprint(*args, **kwargs):
    '''Print Wrapper to add timestamp and hostname to print or tqdm.write() function.'''
    t = time.localtime()
    prefix = "[" + timestamp_8601(t) + "]"
    try:
        tqdm.write(prefix, *args, **kwargs)
    except Exception:
        print(prefix, *args, **kwargs)


def get_timezone():
    tz = str(time.tzname[time.daylight])
    if tz == ('PDT' or 'PST'):
        tz = "America/Los_Angeles"
    return tz


def timestamp(tz):
    tz = pytz.timezone(tz)
    utcnow = datetime.datetime.utcnow()
    ts = utcnow.replace(tzinfo=pytz.utc)
    lt = ts.astimezone(tz)

    time_dict = {
        "Timestamp": ts.strftime("%Y-%m-%d %H:%M:%S%z"),
        "LocalTime": lt.strftime("%Y-%m-%d %H:%M:%S%z"),
        "Datetime": int(ts.strftime("%s")) * 1000
    }

    return time_dict


init_files()
init_keys()
