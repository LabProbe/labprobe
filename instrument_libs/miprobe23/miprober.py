#!/usr/bin/env python3
#cython: language_level=3
from mp_lib import *
import threading

log_data = []

init_serial()
init_board("B56")

thread1 = threading.Thread(target=init_reading_schedule)
thread2 = threading.Thread(target=init_control_schedule)
thread3 = threading.Thread(target=init_heartbeat_schedule)
thread4 = threading.Thread(target=init_cloud_schedule)

thread1.start()
thread2.start()
thread3.start()
thread4.start()

time.sleep(2)

while True:
    run_pending_schedules()
    time.sleep(1)