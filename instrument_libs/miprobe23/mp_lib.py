from mp_basic import *
import socket
import datetime
import schedule
import threading
import urllib3

# File lock for preventing data corruption from multiple schedules.
file_lock = threading.Lock()

# Schedule for reading measurements of sensors
read_schedule = schedule.Scheduler()
read_lock = threading.Lock()

# Schedule for logging data to cloud
cloud_schedule = schedule.Scheduler()
cloud_lock = threading.Lock()

# Schedule for running relays / smart analyses
control_schedule = schedule.Scheduler()
control_lock = threading.Lock()

# Keep-alive schedule for USB-devices, internet connectivity, etc.
heart_beat = schedule.Scheduler()

# Readings Log
log_data = []
diagnostics_data = []
control_data = []

# Connection Testing IP
test_ip = "1.1.1.1"
test_port = 53


def upload_data(data_dict, api_key, api_url):
    timeout = urllib3.Timeout(connect=10.0, read=10.0)
    http = urllib3.PoolManager(timeout=timeout)
    payload = json.dumps(data_dict, indent=4, sort_keys=True)
    http.request('POST', api_url, headers={'Content-Type': 'application/json', 'x-api-key': api_key}, body=payload)


def read_config(filename):
    try:
        file_lock.acquire()
        with open(filename) as data:
            config = json.load(data)
            data.close()
            file_lock.release()
            return config
    except FileNotFoundError:
        file_lock.release()
        pass


def get_logger_settings():
    logger_config = {
        "device": config['model'],
        "experimentName": config['labMode']['experimentName'],
        "cloud_mode": config['labMode']['modes']['cloud'],
        "network_mode": config['labMode']['modes']['network'],
        "interactive_mode": config['labMode']['modes']['interactive'],
        "sample_mode": config['labMode']['sampling']['mode'],
        "oversampling": config['labMode']['sampling']['oversampling'],
        "schedules": config['labMode']['schedules']
    }
    return logger_config


def get_schedule_settings(schedule_name):
    settings = config['schedules'][schedule_name]
    return settings


def parse_schedule_inputs(schedule_n):
    inputs = config['schedules'][schedule_n]['inputs']
    sensor_list = []
    for i in inputs:
        if inputs[i] == True:
            sensor_list.append(i)
    return(sensor_list)


def read_data(schedule_n, schedule_name):
    ts = timestamp(config['location']['timezone'])
    board = config['model']

    miprobe_sensors = []

    # Garbage to support older serial boards and their different sensor naming schemes and serial commands.
    if board == "B10":
        command = get_query_command(board, "ALL")[board]['ALL']

    for i in config['schedules'][schedule_n]['inputs']:
        if config['schedules'][schedule_n]['inputs'][i] == True:
            miprobe_sensors.append(i)

    command = get_query_command(board, miprobe_sensors)[board]['QUERY']

    if board == "B56":
        readings = read_B56(command)
    if board == "B176":
        readings = read_B176(command)
    if board == "B10":
        readings = read_B10(command)

    names = config['sensorNames']
    for i in names.keys():
        if i in readings.keys():
            name = names[i]
            readings[name] = readings.pop(i)

    xprint("Gathering Sensor Inputs.")
    data = {**ts, **readings}
    data['Schedule'] = str(schedule_name)

    read_lock.acquire()
    log_data.append(data)
    read_lock.release()

    filename = str(config['deviceName']) + "-" + str(schedule_name) + ".csv"
    file_lock.acquire()
    dict_to_csv(data, filename)
    file_lock.release()

    cloud_lock.acquire()
    data['Status'] = "Queued"
    dict_to_jsonfile(data, "queue.json")
    cloud_lock.release()


def output_on(output):
    xprint("Turning on output: " + str(output))


def control_output(schedule_n, schedule_name):
    outputs = config['schedules'][schedule_n]['outputs']

    for x in outputs:
        relay_data = {}
        ts = timestamp(config['location']['timezone'])
        if outputs[x] == True:
            relay_data['relay'] = x
            output_on(x)
        relay_data['Schedule'] = str(schedule_name)
    data = {**ts, **relay_data}

    read_lock.acquire()
    log_data.append(data)
    read_lock.release()

    filename = str(config['deviceName']) + "-" + str(schedule_name) + ".csv"
    file_lock.acquire()
    dict_to_csv(data, filename)
    file_lock.release()


def parse_read_schedule(schedule_config, schedule_n):
    global read_schedule
    
    if schedule_config['name'] in ['data', 'diagnostics']:

        if config['model'] == "B176":
            init_second = ":56"
        if config['model'] == "B56":
            init_second = ":57"
        if config['model'] == "B10":
            init_second = ":58"
        else:
            init_second = ":59"
        
        interval = schedule_config['interval']
        start_time = schedule_config['startTime']
        days_of_week = schedule_config['days']
        name = schedule_config['name']
        
        # Check if all days are selected in schedule
        daily = [True for match in ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'] if match in days_of_week]
        
        def job(day, schedule_n, schedule_name):
            if datetime.datetime.today().weekday() == day:
                read_data(schedule_n=schedule_n, schedule_name=schedule_name)    

        if interval >= 60:
            interval_minutes = interval / 60    
            if all(daily) == True:
                read_schedule.every(interval_minutes).minutes.at(init_second).do(read_data, schedule_n=schedule_n, schedule_name=name)
            else:
                if 'Mo' in days_of_week:
                    read_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=0, schedule_n=schedule_n, schedule_name=name)
                if 'Tu' in days_of_week:
                    read_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=1, schedule_n=schedule_n, schedule_name=name)
                if 'We' in days_of_week:
                    read_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=2, schedule_n=schedule_n, schedule_name=name)
                if 'Th' in days_of_week:
                    read_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=3, schedule_n=schedule_n, schedule_name=name)
                if 'Fr' in days_of_week:
                    read_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=4, schedule_n=schedule_n, schedule_name=name)
                if 'Sa' in days_of_week:
                    read_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=5, schedule_n=schedule_n, schedule_name=name)
                if 'Su' in days_of_week:
                    read_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=6, schedule_n=schedule_n, schedule_name=name)
        else:
            if all(daily) == True:
                read_schedule.every(interval).seconds.do(read_data, schedule_n=schedule_n, schedule_name=name)
            else:
                if 'Mo' in days_of_week:
                    read_schedule.every(interval).seconds.do(job, day=0, schedule_n=schedule_n, schedule_name=name)
                if 'Tu' in days_of_week:
                    read_schedule.every(interval).seconds.do(job, day=1, schedule_n=schedule_n, schedule_name=name)
                if 'We' in days_of_week:
                    read_schedule.every(interval).seconds.do(job, day=2, schedule_n=schedule_n, schedule_name=name)
                if 'Th' in days_of_week:
                    read_schedule.every(interval).seconds.do(job, day=3, schedule_n=schedule_n, schedule_name=name)
                if 'Fr' in days_of_week:
                    read_schedule.every(interval).seconds.do(job, day=4, schedule_n=schedule_n, schedule_name=name)
                if 'Sa' in days_of_week:
                    read_schedule.every(interval).seconds.do(job, day=5, schedule_n=schedule_n, schedule_name=name)
                if 'Su' in days_of_week:
                    read_schedule.every(interval).seconds.do(job, day=6, schedule_n=schedule_n, schedule_name=name)


def parse_control_schedule(schedule_config, schedule_n):
    global control_schedule
    
    if schedule_config['name'] not in ['data', 'diagnostics']:
        init_second = ":59"
        
        interval = schedule_config['interval']
        start_time = schedule_config['startTime']
        days_of_week = schedule_config['days']
        name = schedule_config['name']
        
        # Check if all days are selected in schedule
        daily = [True for match in ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'] if match in days_of_week]

        def job(day, schedule_n, schedule_name):
            if datetime.datetime.today().weekday() == day:
                control_output(schedule_n=schedule_n, schedule_name=schedule_name)    

        if interval >= 60:
            interval_minutes = interval / 60    
            if all(daily) == True:
                control_schedule.every(interval_minutes).minutes.at(init_second).do(control_output, schedule_n=schedule_n, schedule_name=name)
            else:
                if 'Mo' in days_of_week:
                    control_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=0, schedule_n=schedule_n, schedule_name=name)
                if 'Tu' in days_of_week:
                    control_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=1, schedule_n=schedule_n, schedule_name=name)
                if 'We' in days_of_week:
                    control_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=2, schedule_n=schedule_n, schedule_name=name)
                if 'Th' in days_of_week:
                    control_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=3, schedule_n=schedule_n, schedule_name=name)
                if 'Fr' in days_of_week:
                    control_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=4, schedule_n=schedule_n, schedule_name=name)
                if 'Sa' in days_of_week:
                    control_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=5, schedule_n=schedule_n, schedule_name=name)
                if 'Su' in days_of_week:
                    control_schedule.every(interval_minutes).minutes.at(init_second).do(job, day=6, schedule_n=schedule_n, schedule_name=name)
        else:
            if all(daily) == True:
                control_schedule.every(interval).seconds.do(control_output, schedule_n=schedule_n, schedule_name=name)
            else:
                if 'Mo' in days_of_week:
                    control_schedule.every(interval).seconds.do(job, day=0, schedule_n=schedule_n, schedule_name=name)
                if 'Tu' in days_of_week:
                    control_schedule.every(interval).seconds.do(job, day=1, schedule_n=schedule_n, schedule_name=name)
                if 'We' in days_of_week:
                    control_schedule.every(interval).seconds.do(job, day=2, schedule_n=schedule_n, schedule_name=name)
                if 'Th' in days_of_week:
                    control_schedule.every(interval).seconds.do(job, day=3, schedule_n=schedule_n, schedule_name=name)
                if 'Fr' in days_of_week:
                    control_schedule.every(interval).seconds.do(job, day=4, schedule_n=schedule_n, schedule_name=name)
                if 'Sa' in days_of_week:
                    control_schedule.every(interval).seconds.do(job, day=5, schedule_n=schedule_n, schedule_name=name)
                if 'Su' in days_of_week:
                    control_schedule.every(interval).seconds.do(job, day=6, schedule_n=schedule_n, schedule_name=name)

                
def init_reading_schedule():
    global config
    config = read_config('config.json')
    for i in config['schedules']:
        parse_read_schedule(config['schedules'][i], i)


def init_control_schedule():
    global config
    config = read_config('config.json')
    for i in config['schedules']:
        parse_control_schedule(config['schedules'][i], i)


def init_heartbeat_schedule(log_console=True):
    def job():
        # Check USB
        try:
            if mp_usb.ser.isOpen() == True:
                if log_console == True:
                    xprint("Serial Status: OK")
        except Exception as e:
            xprint(e)
            xprint("USB Device Disconnected.  Attempting to Reinitialize.")
            log_error("Device_Error", e)
            init_serial()
        try:
            socket.create_connection((test_ip, test_port))
        except OSError as e:
            xprint(e)
            xprint("Check Internet Connectivity")
            log_error("Network_Error", e)
        console_messages()
    heart_beat.every(1).minute.at(":30").do(job)


def init_cloud_schedule():
    cloud_config = read_config('config.json')
    if cloud_config['labMode']['modes']['cloud'] == True:
        xprint("Cloud Logging Enabled.")


def console_messages():
    if len(log_data) < 1:
        for i in read_schedule.get_jobs():
            xprint(i)
        for i in cloud_schedule.get_jobs():
            xprint(i)
        for i in control_schedule.get_jobs():
            xprint(i)
    #xprint(str(len(log_data)) + " records logged.")


def run_pending_schedules():
    read_schedule.run_pending()
    cloud_schedule.run_pending()
    control_schedule.run_pending()
    heart_beat.run_pending()