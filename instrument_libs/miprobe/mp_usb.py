# Functions related to MiProbe USB-Serial Devices
#cython: language_level=3
import os
import serial
import serial.tools.list_ports
import time
import simplejson as json
from mp_basic import *


def init_serial(baud_rate=38400):
    global ser
    global serial_port
    serial_ports = serial.tools.list_ports.comports()
    board_ports = []

    for i, p in enumerate(serial_ports):
        if "ttyacm" in str(p).lower():  # Linux
            board_ports.append(str(p).split(" ")[0])
        if "cu.usbmodem" in str(p).lower():  # Mac OS X
            board_ports.append(str(p).split(" ")[0])
        if "U" in str(p):  # FreeBSD
            if "cuau" in str(p).lower():
                board_ports.append(str(p).split(" ")[0])
        if os.name == 'nt':  # Windows
            if "com" in str(p).lower():
                board_ports.append(str(p).split(" ")[0])

    if len(board_ports) == 0:
        xprint("No Serial Devices Found.  Check USB & Power Connections")
    elif len(board_ports) >= 2:
        xprint("Multiple devices found.  Please select MiProbe Serial Port.")
        xprint("Port Number\t" + "Port")
        for index, port in enumerate(board_ports):
            xprint(str(index) + " \t\t" + str(port))
        selected_port = int(input("Enter Port Number: "))
        serial_port = str(board_ports[selected_port])
    else:
        xprint("MiProbe Device found on port: " + str(board_ports[0]))
        serial_port = str(board_ports[0])

    ser = serial.Serial(serial_port, baud_rate)
    if ser.isOpen():
        ser.flushInput()
        ser.flushOutput()
        xprint("Serial connection established with MiProbe system board.")
    else:
        xprint("Cannot establish connection with " + str(serial_port) + ".")
        xprint("Check if another application is useding the serial port.")
    
    return ser, serial_port


def init_board(board_type):
    global query_sleep
    query_sleep = .5
    if board_type == "B10":
        command_unlock = "&LF1=1"
        ser.write(command_unlock.encode('ascii'))
        time.sleep(2)
        command_lab_mode = "&PI4=2"
        ser.write(command_lab_mode.encode('ascii'))
        time.sleep(2)
        ser.flushOutput()
        ser.flushInput()
        time.sleep(1)
        query_sleep = 1
    if board_type == "B176":
        query_sleep = 5


def convert_input_names(board, input_list):
    sensor_list = []
    if board in ['B56', 'B56D']:
        for i in input_list:
            prefix = i[0]
            number = i[1:]
            name = i[0] + i[2:]
            sensor_list.append(name)
    if board in ['B176', 'B176D']:
        for i in input_list:
            prefix = i[0]
            number = i[1:]
            name = i[0] + i[1:]
            sensor_list.append(name)
    if board == 'B10':
        prefix = "&"
        for i in input_list:
            if i[0] == "N":
                number = i[1:]
                name = prefix + "NV" + number[-1:]
                sensor_list.append(name)
            if i[0] == "T":
                number = i[1:]
                name = prefix + "NT" + number[-1:]
                sensor_list.append(name)
            if i[0] == "A":
                number = i[1:]
                name = prefix + "SR" + number[-1:]
                sensor_list.append(name)
    return sensor_list


def get_node_list(prefix, number):
    nodes = []
    for i in range(number):
        i = i + 1
        prefix = prefix
        number = zfl(str(i), 4)
        name = prefix + number
        nodes.append(name)
    return nodes


# Functions that generate Serial Port Command Strings
def get_query_command(board, input_list):
    commands = {
        board: {
        }
    }
    command = "QUERY"
    if type(input_list) != list:
        command = input_list
    if board in ["B56", "B56D"]:
        if input_list == "ALLV":
            nodes = get_node_list("N", 56)
            sensors = convert_input_names(board, nodes)
        if input_list == "ALLT":
            nodes = get_node_list("T", 5)
            sensors = convert_input_names(board, nodes)
        if input_list == "ALLVT":
            nodes = get_node_list("N", 56)
            nodes = nodes + get_node_list("T", 5)
            sensors = convert_input_names(board, nodes)
        else:
            sensors = convert_input_names(board, input_list)
        commands[board][command] = sensors
    if board in ["B176", "B176D"]:
        if input_list == "ALLV":
            nodes = get_node_list("N", 176)
            sensors = convert_input_names(board, nodes)
        if input_list == "ALLT":
            nodes = get_node_list("T", 5)
            sensors = convert_input_names(board, nodes)
        if input_list == "ALLVT":
            nodes = get_node_list("N", 176)
            nodes = nodes + get_node_list("T", 5)
            sensors = convert_input_names(board, nodes)
        else:
            sensors = convert_input_names(board, input_list)
        commands[board][command] = sensors
    if board == "B10":
        if input_list == "ALLV":
            nodes = get_node_list("N", 8)
            sensors = convert_input_names(board, nodes)
        if input_list == "ALLT":
            nodes = get_node_list("T", 8)
            sensors = convert_input_names(board, nodes)
        if input_list == "ALLVT":
            nodes = get_node_list("V", 8)
            nodes = nodes + get_node_list("T", 8)
            sensors = convert_input_names(board, nodes)
        if input_list == "ALL":
            nodes = get_node_list("V", 8)
            nodes = nodes + get_node_list("T", 8)
            nodes = nodes + get_node_list("A", 8)
            sensors = convert_input_names(board, nodes)            
        else:
            sensors = convert_input_names(board, input_list)
        commands[board][command] = ''.join(map(str, sensors))
    return commands


def read_B10(sensor_list):
    ser.flushOutput()
    ser.flushInput()
    if sensor_list == "ALL":
        command = commands['B10']['ALL']
    if sensor_list == "ALLVT":
        command = commands['B10']['ALLVT']
    if sensor_list == "ALLV":
        command = commands['B10']['ALLV']
    else:
        command = command['B10']['QUERY']
    query = str("\\" + command + "\r\n")
    ser.write(query.encode('ascii'))
    time.sleep(query_sleep)
    response = str(ser.read(ser.inWaiting()).decode()).strip('\r\n')
    response = response.strip("  Unlock_1 =1")
    reading_dict = ast.literal_eval(response)
    data = json.loads(json.dumps(readings_dict, indent=4, sort_keys=True))
    return data


def read_B56(sensor_list):
    xprint("Reading from B56")
    ser.flushOutput()
    ser.flushInput()
    if sensor_list == "ALL":
        command = commands['B56']['ALL']
    if sensor_list == "ALLVT":
        command = commands['B56']['ALLVT']
    if sensor_list == "ALLV":
        command = commands['B56']['ALLV']
    else:
        command = ""
    for i in sensor_list:
        command = command + str(i) + ","
    query = str("\\" + command + "\r\n")
    ser.write(query.encode('ascii'))
    time.sleep(query_sleep)
    response = str(ser.read(ser.inWaiting()).decode()).strip('\r\n')
    if response.endswith(',') is True:
        response = response[:-1]
    readings = list(response.split(","))
    headers = sensor_list
    readings_dict = dict(zip(headers, readings))
    ser.flushInput()
    ser.flushOutput()
    data = json.loads(json.dumps(readings_dict, indent=4, sort_keys=True))
    return data


def read_B176(sensor_list):
    ser.flushOutput()
    ser.flushInput()
    if sensor_list == "ALL":
        sensor_list = commands['B176']['ALL']
    if sensor_list == "ALLVT":
        sensor_list = commands['B176']['ALLVT']
    if sensor_list == "ALLV":
        sensor_list = commands['B176']['ALLV']
    else:
        command = ""
    for i in sensor_list:
        command = command + str(i) + ","
    query = str("\\" + command + "\r\n")
    ser.write(query.encode('ascii'))
    time.sleep(query_sleep)
    response = str(ser.read(ser.inWaiting()).decode()).strip('\r\n')
    if response.endswith(',') is True:
        response = response[:-1]
    readings = list(response.split(","))
    headers = list(command.split(","))
    readings_dict = dict(zip(headers, readings))
    ser.flushInput()
    ser.flushOutput()
    data = json.loads(json.dumps(readings_dict, indent=4, sort_keys=True))
    return data