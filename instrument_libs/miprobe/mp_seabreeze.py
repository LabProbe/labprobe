import seabreeze
seabreeze.use('cseabreeze')
from seabreeze.spectrometers import list_devices, Spectrometer
import time
import struct


def init_seabreeze():
    global devices
    devices = []
    for i in list_devices():
        device_name = str(i)[-9:][:-1]
        devices.append(device_name)
    return devices


def get_spectra(device, channel):
    spec = Spectrometer.from_serial_number(device)
    spec.f.raw_usb_bus_access.raw_usb_write(data=struct.pack('<BB', 0xC1, channel), endpoint='primary_out')
    time.sleep(.5)  # Not necessary?
    wavelengths = spec.wavelengths()
    intensities = spec.intensities()
    reset_channel(spec)
    spec.close()
    data = dict(zip(wavelengths, intensities))
    return data
    

def reset_channel(spec):
    channel = 0
    spec.f.raw_usb_bus_access.raw_usb_write(data=struct.pack('<BB', 0xC1, channel), endpoint='primary_out')

# This function does not work, some sort of timeout issue on usb.  
# This follows the documentation of pyseabreeze and the JAZ documentation.
def get_channels(device):
    spec = Spectrometer.from_serial_number(device)
    spec.f.raw_usb_bus_access.raw_usb_write(data=struct.pack('<B', 0xC0), endpoint='primary_out')
    data = spec.f.raw_usb_bus_access.raw_usb_read(endpoint='primary_in')
    num_channels = struct.unpack('<B', data)
    return num_channels