# Alicat RS232/Serial CO2 flowmeter library
# Alicat's python libraries don't work correctly, so this is a complete rewrite based off MiProbe/LabProbe serial libraries
import serial
import serial.tools.list_ports
import os
import time
import simplejson as json

def timestamp_8601(time):
    t = time
    date = str(zfl(str(t[0]), 4) + "-" + zfl(str(t[1]), 2) + "-" + zfl(str(t[2]), 2))
    time = str(zfl(str(t[3]), 2) + ":" + zfl(str(t[4]), 2) + ":" + zfl(str(t[5]), 2))
    return str(date + " " + time)


def zfl(s, width):
    '''Replaces .zfill on string for python and micropython compatibiity'''
    return '{:0>{w}}'.format(s, w=width)


def xprint(*args, **kwargs):
    '''Print Wrapper to add timestamp and hostname to print or tqdm.write() function.'''
    t = time.localtime()
    prefix = "[" + timestamp_8601(t) + "]"
    try:
        tqdm.write(prefix, *args, **kwargs)
    except Exception:
        print(prefix, *args, **kwargs)


def get_timezone():
    tz = str(time.tzname[time.daylight])
    if tz == ('PDT' or 'PST'):
        tz = "America/Los_Angeles"
    return tz


def init_serial(baud_rate=19200):
    global ser
    global serial_port
    serial_ports = serial.tools.list_ports.comports()
    board_ports = []

    for i, p in enumerate(serial_ports):
        if "ttyacm" in str(p).lower():  # Linux
            board_ports.append(str(p).split(" ")[0])
        if "ttyusb" in str(p).lower():  # Linux
            board_ports.append(str(p).split(" ")[0])
        if "cu.usbmodem" in str(p).lower():  # Mac OS X
            board_ports.append(str(p).split(" ")[0])
        if "U" in str(p):  # FreeBSD
            if "cuau" in str(p).lower():
                board_ports.append(str(p).split(" ")[0])
        if os.name == 'nt':  # Windows
            if "com" in str(p).lower():
                board_ports.append(str(p).split(" ")[0])

    if len(board_ports) == 0:
        xprint("No Serial Devices Found.  Check USB & Power Connections")
    elif len(board_ports) >= 2:
        xprint("Multiple devices found.  Please select Alicat Serial Port.")
        xprint("Port Number\t" + "Port")
        for index, port in enumerate(board_ports):
            xprint(str(index) + " \t\t" + str(port))
        selected_port = int(input("Enter Port Number: "))
        serial_port = str(board_ports[selected_port])
    else:
        xprint("MiProbe Device found on port: " + str(board_ports[0]))
        serial_port = str(board_ports[0])

    ser = serial.Serial(serial_port, baud_rate)
    if ser.isOpen():
        ser.flushInput()
        ser.flushOutput()
        xprint("Serial connection established with MiProbe system board.")
    else:
        xprint("Cannot establish connection with " + str(serial_port) + ".")
        xprint("Check if another application is useding the serial port.")
    
    return ser, serial_port


def read_alicat(address, sleep):
    keys = ['Address', 'pressure', 'temperature', 'volumetric_flow', 'mass_flow', 'total_flow', 'gas', 'status_code']
    ser.flushOutput()
    ser.flushInput()
    query = str(address + "\r")
    ser.write(query.encode('ascii'))
    time.sleep(sleep)
    response = str(ser.read(ser.inWaiting()).decode())
    readings = response.split()
    data = dict(zip(keys, readings))
    return data


def tare_flow(address, sleep):
    ser.flushOutput()
    ser.flushInput()
    query = str(address +"V" + "\r")
    ser.write(query.encode('ascii'))
    time.sleep(sleep)